# API usage
REST endpoints: `/generate_code` for generating a PNG image containing the Aztec Code, `/generate_data` to generate the binary data stored in the barcode for downloading.

## POST request JSON format
Example structure for generating a normal RCT2 style ticket:
```json
{
  "key_id": "7RE01",
  "rics_issuer": 1234,
  "aztec_data": {
    "customer_id": "12345",
    "date_of_purchase": "14.01.18",
    "description": "Test Ticket",
    "from": "Hauptbahnhof",
    "name": "Max Mustermann",
    "number_of_adults": 1,
    "number_of_children": 0,
    "order_number": "12340-0001-01",
    "pass": "NOBAHNCARD",
    "service_class": 2,
    "to": "Südbahnhof",
    "transaction_number": "12345678"
  },
  "rct2_data": {
    "control_medium": "Personalausweis",
    "currency_unit": "EUR",
    "customer_type": "Personen",
    "date_of_birth": "26.04.1987",
    "from": "Hauptbahnhof",
    "given_name": "Max",
    "last_name": "Mustermann",
    "num_customers": 1,
    "price": "6,90",
    "price_bracket": 7,
    "product_name": "Luftlinientarif Regeltarif",
    "tariff_zone": "",
    "ticket_type": "Fahrtberechtigung",
    "to": "Südbahnhof",
    "validity_text": "Gültig ab 14.01.2018 18:45 Uhr bis 14.01.2018",
    "validity_year": "2018",
    "valid_from_date": "14.01",
    "valid_from_time": "18:45",
    "valid_until_date": "14.01",
    "valid_until_time": "23:59"
  }
}
```

### Aztec Code data fields
The `aztec_data` parameter specifies the data to be used for the Aztec Code.

Attribute | Description | Allowed values
--- | --- | ---
`customer_id` | The customer ID of the passenger who has booked the ticket | Up to 7 characters
`date_of_purchase` | The date when the ticket was booked | Up to 8 characters
`description` | Some description of the ticket, usually its name | Up to 20 characters
`from` | The stop or station from which the ticket is valid | Up to 20 characters
`name` | The name of the passenger who has booked the ticket | Up to 20 characters
`number_of_adults` | The number of adults travelling with the ticket | Integer between 0 and 99
`number_of_children` | The number of children travelling with the ticket | Integer between 0 and 99
`order_number` | A unique identifier for the ticket | Up to 20 characters
`pass` | Some text specifying an identification document | Up to 10 characters
`service_class` | The class the passenger is allowed to travel in | Integer between 0 and 9
`to` | The stop or station until which the ticket is valid | Up to 20 characters
`transaction_number` | Some number identifying the booking, but seems to be hardcoded to 12345678 | Up to 8 characters

### RCT2 Ticket data fields
The `rct2_data` parameter specifies the data to be used for the RCT2 Ticket. These values should all be in German.

Attribute | Description | Allowed values
--- | --- | ---
`control_medium` | The document used to identify the passenger, e.g. "Personalausweis" (ID card) | Up to 25 characters
`currency_unit` | The short code for the currency used in the transaction (e.g. "EUR") | Up to 3 characters
`customer_type` | What group the passenger belongs to, e.g. "Erwachsene" or "Kinder", but "Personen" is also okay | Up to 16 characters
`date_of_birth` | The date of birth of the passenger in the format dd.mm.yyyy | Up to 10 characters
`from` | The beginning stop of the journey | Up to 20 characters
`given_name` | The given name of the passenger | Up to 9 characters
`last_name` | The last name of the passenger | Up to 9 characters
`num_customers` | The number of passengers travelling | Up to 2 characters
`price` | The price of the ticket, without currency unit, e.g. "6,90" | Up to 15 characters
`price_bracket` | The price bracket of the ticket, usually a number | Up to 24 characters
`product_name` | The name of the ticket | Up to 33 characters
`tariff_zone` | The tariff zone of the ticket, seems to be unused in this case | Up to 33 characters
`ticket_type` | The type of RCT2 document, usually "Fahrtberechtigung" (fancy term for "ticket") | Up to 33 characters
`to` | The end stop of the journey, should be empty in this case | Up to 17 characters
`validity_text` | A text describing the validity of the ticket, usually containing the dates and times the ticket is valid | Up to 2x50 characters
`validity_year` | The year the ticket is valid | Up to 4 characters
`valid_from_date` | The beginning date of the ticket's validity | Up to 5 characters
`valid_from_time` | The beginning time of the ticket's validity | Up to 5 characters
`valid_until_date` | The end date of the ticket's validity, should be empty in this case | Up to 5 characters
`valid_until_time` | The end time of the ticket's validity, should be empty in this case | Up to 5 characters

### Other data fields

Attribute | Description | Allowed values
--- | --- | ---
`key_id` | A string identifying the key used for signing the ticket | Exactly 5 characters
`rics_issuer` | The RICS code of the company issuing the ticket | Integer between 0 and 9999

## Return value
On success, the API endpoint `/generate_code` will return raw PNG image data for the Aztec Code.
The API endpoint `/generate_data` will return the raw binary ticket data stored in the Aztec Code.
On error, it will return HTTP 500.

# Docker container
The docker container can be pulled with `docker pull sevenre/code_generator:latest` and run with `docker run -p 3452:5000 sevenre/code_generator`.