from flask import Flask, Response, request
from io import BytesIO
import time
import uic_918_3

def _generate_rct2_fields(data):
    fields = []
    fields.append({
          "text": data['ticket_type'][:33],
          "width": 33,
          "line": 0,
          "format": 0,
          "column": 18,
          "height": 1
        })
    fields.append({
          "text": data['last_name'][:9],
          "width": 9,
          "line": 0,
          "format": 0,
          "column": 52,
          "height": 1
        })
    fields.append({
          "text": data['given_name'][:9],
          "width": 9,
          "line": 0,
          "format": 0,
          "column": 62,
          "height": 1
        })
    fields.append({
          "text": data['product_name'][:33],
          "width": 33,
          "line": 1,
          "format": 0,
          "column": 18,
          "height": 1
        })
    fields.append({
          "text": str(data['num_customers'])[:2],
          "width": 2,
          "line": 1,
          "format": 0,
          "column": 52,
          "height": 1
        })
    fields.append({
          "text": data['customer_type'][:16],
          "width": 16,
          "line": 1,
          "format": 0,
          "column": 55,
          "height": 1
        })
    fields.append({
          "text": str(data['validity_year'])[:4],
          "width": 4,
          "line": 3,
          "format": 0,
          "column": 1,
          "height": 1
        })
    fields.append({
          "text": data['date_of_birth'][:10],
          "width": 10,
          "line": 3,
          "format": 0,
          "column": 52,
          "height": 1
        })
    fields.append({
          "text": data['valid_from_date'][:5],
          "width": 5,
          "line": 6,
          "format": 0,
          "column": 1,
          "height": 1
        })
    fields.append({
          "text": data['valid_from_time'][:5],
          "width": 5,
          "line": 6,
          "format": 0,
          "column": 7,
          "height": 1
        })
    fields.append({
          "text": data['from'][:20],
          "width": 20,
          "line": 6,
          "format": 0,
          "column": 13,
          "height": 1
        })
    fields.append({
          "text": data['to'][:17],
          "width": 17,
          "line": 6,
          "format": 0,
          "column": 34,
          "height": 1
        })
    fields.append({
          "text": data['valid_until_date'][:5],
          "width": 5,
          "line": 6,
          "format": 0,
          "column": 52,
          "height": 1
        })
    fields.append({
          "text": data['valid_until_time'][:5],
          "width": 5,
          "line": 6,
          "format": 0,
          "column": 58,
          "height": 1
        })
    fields.append({
          "text": data['validity_text'][:50],
          "width": 50,
          "line": 12,
          "format": 0,
          "column": 1,
          "height": 2
        })
    fields.append({
          "text": data['currency_unit'][:3],
          "width": 3,
          "line": 13,
          "format": 0,
          "column": 52,
          "height": 1
        })
    fields.append({
          "text": str(data['price'])[:15],
          "width": 15,
          "line": 13,
          "format": 0,
          "column": 56,
          "height": 1
        })
    fields.append({
          "text": data['control_medium'][:25],
          "width": 25,
          "line": 14,
          "format": 0,
          "column": 1,
          "height": 1
        })
    fields.append({
          "text": str(data['price_bracket'])[:24],
          "width": 24,
          "line": 14,
          "format": 0,
          "column": 27,
          "height": 1
        })
    fields.append({
          "text": str(data['tariff_zone'])[:33],
          "width": 33,
          "line": 3,
          "format": 0,
          "column": 18,
          "height": 1
        })
    return fields

app = Flask(__name__)

@app.route("/")
def root():
    return Response("SevenRE UIC Aztec code generator", mimetype = 'text/plain')

@app.route("/generate_code", methods = ['POST'])
def generate_code():
    data = request.get_json()
    aztec_data = data['aztec_data']
    rct2_fields = _generate_rct2_fields(data['rct2_data'])
    gen = uic_918_3.UIC9183(msg_type="#UT", type_ver=1, rics=data['rics_issuer'], key_id=data['key_id'], key_file="key.pem")
    gen.add_record_u_head(ticket_id=aztec_data['order_number'], timestamp=time.strftime("%d%m%Y%H%M"), flags=0, lang_pri="de", lang_sec="")
    gen.add_record_u_tlay(layout="RCT2", fields=rct2_fields)
    gen.add_record_1180ai(
        customer_no=aztec_data['customer_id'],
        transaction_no=aztec_data['transaction_number'],
        name=aztec_data['name'],
        num_adults=aztec_data['number_of_adults'],
        num_children=aztec_data['number_of_children'],
        description=aztec_data['description'],
        identification=aztec_data['pass'],
        valid_from_date=aztec_data['date_of_purchase'],
        valid_to_date="",
        valid_from_stop=aztec_data['from'],
        valid_to_stop=aztec_data['to'],
        travel_class=int(aztec_data['service_class']),
        issue_date=aztec_data['date_of_purchase']
    )
    code = gen.build_aztec_code()
    output = BytesIO()
    code.save(output, format = 'PNG')
    return Response(output.getvalue(), mimetype = 'image/png')

@app.route("/generate_data", methods = ['POST'])
def generate_data():
    data = request.get_json()
    aztec_data = data['aztec_data']
    rct2_fields = _generate_rct2_fields(data['rct2_data'])
    gen = uic_918_3.UIC9183(msg_type="#UT", type_ver=1, rics=data['rics_issuer'], key_id=data['key_id'], key_file="key.pem")
    gen.add_record_u_head(ticket_id=aztec_data['order_number'], timestamp=time.strftime("%d%m%Y%H%M"), flags=0, lang_pri="de", lang_sec="")
    gen.add_record_u_tlay(layout="RCT2", fields=rct2_fields)
    gen.add_record_1180ai(
        customer_no=aztec_data['customer_id'],
        transaction_no=aztec_data['transaction_number'],
        name=aztec_data['name'],
        num_adults=aztec_data['number_of_adults'],
        num_children=aztec_data['number_of_children'],
        description=aztec_data['description'],
        identification=aztec_data['pass'],
        valid_from_date=aztec_data['date_of_purchase'],
        valid_to_date="",
        valid_from_stop=aztec_data['from'],
        valid_to_stop=aztec_data['to'],
        travel_class=int(aztec_data['service_class']),
        issue_date=aztec_data['date_of_purchase']
    )
    code_data = gen.build_aztec_data()
    return Response(code_data, mimetype = 'application/octet-stream',
        headers = {'Content-Disposition': "attachment;filename=ticket.bin"})

if __name__ == "__main__":
    app.run(debug = False, host = "0.0.0.0")