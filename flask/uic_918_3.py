import OpenSSL.crypto
import treepoem
import zlib

class UIC9183:
    ENCODING = 'latin1' # Used as intermediate encoding for format strings
    
    def __init__(self, msg_type, type_ver, rics, key_id, key_file):
        """
        msg_type: str
        type_ver: int
        rics: int
        key_id: str
        key_file: str
        """
        assert len(msg_type) == 3
        assert 0 <= type_ver <= 99
        assert 0 <= rics <= 9999
        assert len(key_id) == 5
        self.msg_type = msg_type
        self.type_ver = type_ver
        self.rics = rics
        self.key_id = key_id
        self.key_file = key_file
        self.data = b""
    
    def build_aztec_code(self):
        """
        Build the aztec code
        """
        aztec_options = {
            'eclevel': 23,
            'format': 'full'
        }
        aztec_data = self.build_aztec_data()
        aztec_code = treepoem.generate_barcode(
            barcode_type = 'azteccode',
            data = aztec_data,
            options = aztec_options
        )
        return aztec_code
    
    def build_aztec_data(self):
        """
        Build the aztec code data
        """
        signature = self.sign(self.data)
        assert len(signature) <= 50
        
        compressed_data = self.deflate(self.data)
        assert len(compressed_data) <= 9999
        
        aztec_data = bytearray()
        aztec_data += "{msg_type}{type_ver:02d}{rics:04d}{key_id}".format(
            msg_type = self.msg_type,
            type_ver = self.type_ver,
            rics = self.rics,
            key_id = self.key_id,
        ).encode(self.ENCODING)
        aztec_data += signature
        aztec_data += bytearray([0x00] * (50 - len(signature)))
        aztec_data += "{:04d}".format(len(compressed_data)).encode(self.ENCODING)
        aztec_data += compressed_data
        return bytes(aztec_data)
    
    def sign(self, data):
        """
        Hash the data and sign the hash with the given private key
        data: bytes
        key_file: str
        """
        with open(self.key_file, 'r') as f:
            key = OpenSSL.crypto.load_privatekey(OpenSSL.crypto.FILETYPE_PEM, f.read())
        return OpenSSL.crypto.sign(key, data, 'sha1')
        
    def deflate(self, data):
        """
        Compress the data using the DEFLATE algorithm
        data: bytes
        """
        deflate_compress = zlib.compressobj(9, zlib.DEFLATED, zlib.MAX_WBITS)
        return deflate_compress.compress(data) + deflate_compress.flush()
    
    def add_record(self, rec_type, type_ver, data):
        """
        Add a general record
        rec_type: str
        type_ver: int
        data: bytes
        """
        assert len(rec_type) == 6
        assert 0 <= type_ver <= 99
        assert len(data) <= 9987
        
        record = "{rec_type}{type_ver:02d}{data_len:04d}{data}".format(
            rec_type = rec_type,
            type_ver = type_ver,
            data_len = len(data) + 12, # include the record header which is of fixed length
            data = data.decode(self.ENCODING)
        )
        self.data += record.encode(self.ENCODING)
    
    def add_record_u_head(self, ticket_id, timestamp, flags, lang_pri, lang_sec):
        """
        Add an U_HEAD record
        ticket_id: str
        timestamp: str
        flags: int
        lang_pri: str
        lang_sec: str
        """
        assert len(ticket_id) <= 20
        assert len(timestamp) == 12
        assert 0 <= flags <= 7
        assert len(lang_pri) <= 2
        assert len(lang_sec) <= 2
        
        data = "{rics:04d}{ticket_id:<20}{timestamp}{flags:01d}{lang_pri:<2}{lang_sec:<2}".format(
            rics = self.rics,
            ticket_id = ticket_id,
            timestamp = timestamp,
            flags = flags,
            lang_pri = lang_pri,
            lang_sec = lang_sec
        )
        self.add_record("U_HEAD", 1, data.encode(self.ENCODING))
    
    def add_record_u_tlay(self, layout, fields):
        """
        Add an U_TLAY record
        layout: str
        fields: list
        """
        assert len(layout) == 4
        assert len(fields) <= 9999
        
        fields_uic = "".join((self.build_field_u_tlay(**field) for field in fields))
        data = "{layout}{field_count:04d}{fields}".format(
            layout = layout,
            field_count = len(fields),
            fields = fields_uic
        )
        self.add_record("U_TLAY", 1, data.encode(self.ENCODING))
    
    def build_field_u_tlay(self, line, column, height, width, format, text):
        """
        Build a field for an U_TLAY record
        line: int
        column: int
        height: int
        width: int
        format: int
        text: str
        """
        assert 0 <= line <= 99
        assert 0 <= column <= 99
        assert 0 <= height <= 99
        assert 0 <= width <= 99
        assert 0 <= format <= 7
        assert len(text) <= 9999
        
        field = "{line:02d}{column:02d}{height:02d}{width:02d}{format:01d}{length:04d}{text}".format(
            line = line,
            column = column,
            height = height,
            width = width,
            format = format,
            length = len(text),
            text = text
        )
        return field
    
    def add_record_1180ai(self, customer_no, transaction_no, name, num_adults, num_children, description, identification, valid_from_date, valid_to_date, valid_from_stop, valid_to_stop, travel_class, issue_date,
        unknown1 = "", unknown2 = "", unknown3 = "", unknown4 = "", unknown5 = "", unknown6 = "", unknown7 = "", unknown8 = ""):
        """
        Add an 1180AI (DB Touch & Travel) record
        customer_no: str
        transaction_no: str
        unknown1: str
        unknown2: str
        name: str
        num_adults: int
        num_children: int
        unknown3: str
        description: str
        identification: str
        unknown4: str
        valid_from_date: str
        valid_to_date: str
        unknown5: str
        valid_from_stop: str
        unknown6: str
        valid_to_stop: str
        travel_class: int
        unknown7: str
        unknown8: str
        issue_date: str
        """
        assert len(customer_no) <= 7
        assert len(transaction_no) <= 8
        assert len(unknown1) <= 5
        assert len(unknown2) <= 2
        assert len(name) <= 20
        assert 0 <= num_adults <= 99
        assert 0 <= num_children <= 99
        assert len(unknown3) <= 2
        assert len(description) <= 20
        assert len(identification) <= 10
        assert len(unknown4) <= 7
        assert len(valid_from_date) <= 8
        assert len(valid_to_date) <= 8
        assert len(unknown5) <= 5
        assert len(valid_from_stop) <= 20
        assert len(unknown6) <= 5
        assert len(valid_to_stop) <= 20
        assert 0 <= travel_class <= 9
        assert len(unknown7) <= 6
        assert len(unknown8) <= 1
        assert len(issue_date) <= 8
        
        data = "{customer_no:<7}{transaction_no:<8}{unknown1:<5}{unknown2:<2}{name:<20}{num_adults:2d}{num_children:2d}{unknown3:<2}{description:<20}{identification:<10}{unknown4:<7}{valid_from_date:<8}{valid_to_date:<8}{unknown5:<5}{valid_from_stop:<20}{unknown6:<5}{valid_to_stop:<20}{travel_class:1d}{unknown7:<6}{unknown8:<1}{issue_date:<8}".format(
            customer_no = customer_no,
            transaction_no = transaction_no,
            unknown1 = unknown1,
            unknown2 = unknown2,
            name = name,
            num_adults = num_adults,
            num_children = num_children,
            unknown3 = unknown3,
            description = description,
            identification = identification,
            unknown4 = unknown4,
            valid_from_date = valid_from_date,
            valid_to_date = valid_to_date,
            unknown5 = unknown5,
            valid_from_stop = valid_from_stop,
            unknown6 = unknown6,
            valid_to_stop = valid_to_stop,
            travel_class = travel_class,
            unknown7 = unknown7,
            unknown8 = unknown8,
            issue_date = issue_date
        )
        self.add_record("1180AI", 1, data.encode(self.ENCODING))