import requests
import time

data = {
  "key_id": "7RE01",
  "rics_issuer": 1234,
  "aztec_data": {
    "customer_id": "12345",
    "date_of_purchase": "14.01.18",
    "description": "Test Ticket",
    "from": "Hauptbahnhof",
    "name": "Max Mustermann",
    "number_of_adults": 1,
    "number_of_children": 0,
    "order_number": "12340-0001-01",
    "pass": "NOBAHNCARD",
    "service_class": 2,
    "to": "Südbahnhof",
    "transaction_number": "12345678"
  },
  "rct2_data": {
    "control_medium": "Personalausweis",
    "currency_unit": "EUR",
    "customer_type": "Personen",
    "date_of_birth": "26.04.1987",
    "from": "Hauptbahnhof",
    "given_name": "Max",
    "last_name": "Mustermann",
    "num_customers": 1,
    "price": "6,90",
    "price_bracket": 7,
    "product_name": "Luftlinientarif Regeltarif",
    "tariff_zone": "",
    "ticket_type": "Fahrtberechtigung",
    "to": "Südbahnhof",
    "validity_text": "Gültig ab 14.01.2018 18:45 Uhr bis 14.01.2018",
    "validity_year": "2018",
    "valid_from_date": "14.01",
    "valid_from_time": "18:45",
    "valid_until_date": "14.01",
    "valid_until_time": "23:59"
  }
}

response = requests.post("http://localhost:5000/generate_code", json = data)
if response.status_code == 200:
    with open("code.png", 'wb') as f:
        for chunk in response:
            f.write(chunk)
else:
    with open("error.txt", 'wb') as f:
        for chunk in response:
            f.write(chunk)

response = requests.post("http://localhost:5000/generate_data", json = data)
if response.status_code == 200:
    with open("code.bin", 'wb') as f:
        for chunk in response:
            f.write(chunk)
else:
    with open("error.txt", 'wb') as f:
        for chunk in response:
            f.write(chunk)